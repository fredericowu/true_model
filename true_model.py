#!/usr/bin/python
# -*- coding: utf-8 -*-
__author__      = "Frederico Wu"
__license__     = "GPL"
__version__     = "0.1"
__email__       = "fredericowu@gmail.com"

########################################
############ DEFINITIONS ###############
########################################

TRUESIGHT_PATH = "/opt/bmc/ProactiveNet"
CELL_NAME = "pncell_brlxpmtz0088" 
BMC_CLASS_CHECK = "BMC_BusinessService,BMC_BusinessProcess,BMC_ComputerSystem"
#BMC_CLASS_CHECK = "BMC_BusinessService"
BMC_CLASS_CREATE = "BMC_BusinessService"

########################################




import logging
import sys
import os

logging.basicConfig(stream=sys.stdout)
LOG = logging.getLogger('logger')
DEBUG = False


def exec_cmd( cmd ):
        cmd = ". %s/pw/server/bin/setup_env.sh; %s" % ( TRUESIGHT_PATH, cmd )
	LOG.debug( "[DEBUG]: EXEC -> %s" % ( cmd ) )

        from subprocess import PIPE, Popen

        process = Popen( args=cmd,  stdout=PIPE, shell=True )
        return process.communicate()[0]




def get_objects ( cell_name, name, dynamic_name, bmc_class, contains = False):
	element = []

	operator = "contains" if contains else "=="
	

	if dynamic_name:
	        result = exec_cmd("mquery -n %s -d -f baroc -a %s -w 'Name: %s \"%s\",ShortDescription: ==\"%s\"'" % ( cell_name, bmc_class, operator, name, dynamic_name ))
	else:
	        result = exec_cmd("mquery -n %s -d -f baroc -a %s -w 'Name: %s \"%s\"'" % ( cell_name, bmc_class, operator, name ))


	for line in result.split("\n"):
		if 'mc_udid=' in line:
			id = line.split("=")[1]
			id = id.replace("'", "")
			id = id.replace(";", "")

			element.append( id )


        return element


def associate_objects( cell_name, consumer, provider ):
	exec_cmd("mposter -n %s -a BMC_Impact -b 'provider_id=%s;consumer_id=%s;PropagationModel=DIRECT' -d -v" % ( cell_name, provider, consumer ) )


def create_object(cell_name, name, dynamic_name, bmc_class ):
	exec_cmd("mposter -n mposter -n %s -a %s -b 'Name=%s;ShortDescription=%s;Category=%s_%s' -d -v" % ( cell_name, bmc_class, name, dynamic_name, dynamic_name, name ) )
	return get_objects ( cell_name, name, dynamic_name, bmc_class )


def remove_object( bmc_class, id, cell_name):
	cmd = "echo \"ddelete %s; mc_udid=%s; END \"| mposter -d -n %s -" % ( bmc_class, id, cell_name )
	#print cmd
	exec_cmd( cmd )


def create_elements( input_file, dynamic_name, create_class, check_class, cell_name, ignore_last_id, remove = False):
	fd = open(input_file)
	lines = fd.readlines()
	fd.close()

	for line in lines:
		line = line.replace("\n", "")
		line = line.replace("\r", "")

		if len(line.strip()) == 0:
			continue

		tree = line.split(" > ")

		tree_elements_order = []
		tree_elements = {}

		c = 0
		for name in tree:
			c = c + 1
			#print name

			DONT_CREATE = False
			CONTAINS = False

			element_objects = []

			if name[0] == "|":
				DONT_CREATE = True
				name = name[1:]

			if name[0] == "~":
				CONTAINS = True
				name = name[1:]


			tree_elements_order.append( name )

			for item in check_class:
				found_objects = []
				if c == len(tree) and ignore_last_id:
					found_objects = get_objects( cell_name, name, None, item, CONTAINS )
				else:
					found_objects = get_objects( cell_name, name, dynamic_name, item, CONTAINS )

				element_objects = element_objects + found_objects

				if remove:
					for id in found_objects:
						remove_object( item, id, cell_name )

			if not remove:
				if not element_objects and not DONT_CREATE:
					element_objects = element_objects + create_object( cell_name, name, dynamic_name, create_class )
			
				if element_objects:
					tree_elements[ name ] = element_objects
				else:
					print "Element not found (%s): %s" % (name, line)


		if len( tree_elements ) == len( tree ) and not remove:
			for index in range( 1, len(tree_elements_order)):
				p_index = index-1

				element_objects = tree_elements[ tree_elements_order[ index ] ]
				p_element_objects = tree_elements[ tree_elements_order[ p_index ] ]
				for p_element_object in p_element_objects:
					for element_object in element_objects:
						associate_objects( cell_name, p_element_object, element_object )


def main(argv):
	global DEBUG
	global TRUESIGHT_PATH
	global CELL_NAME
	global BMC_CLASS_CHECK
	global BMC_CLASS_CREATE

	import argparse

	parser = argparse.ArgumentParser( description='Creates and Associates Components in the Truesight model by Frederico Wu')

	parser.add_argument('-cc','--create-class', help='Class of the element when creating. Default: %s' % BMC_CLASS_CREATE, required=False, default = BMC_CLASS_CREATE )
	parser.add_argument('-ch','--check-class', help='Class of the element when searching, separeted by comma. Default: %s' % BMC_CLASS_CHECK, required=False, default = BMC_CLASS_CHECK )
	parser.add_argument('-cn','--cell-name', help='Name of the Truesight cell. Default: %s' % CELL_NAME, required=False, default = CELL_NAME )

	parser.add_argument('-n','--dynamic-identifier', help='Unique identifier to the tree so no other structure is handled by mistake. Default: DYNAMIC', required=False, default = "DYNAMIC" )
	parser.add_argument('-in','--ignore-last-element-identifier', help='Ignore last element identifier. Good for adding already existing objects to the model. Default: False', required=False, action='store_true' ) 
	parser.add_argument('-rm','--remove', help='Remove objects', required=False, action='store_true' ) 
	parser.add_argument('-p','--truesight-path', help='Set the installation path of Truesight. Default: %s' % TRUESIGHT_PATH, required=False, default = TRUESIGHT_PATH ) 
	parser.add_argument('-d','--debug', help='View debug messages', required=False, action='store_true' ) 
	parser.add_argument('-i','--input-file', help='Input file with the list', required=True )

	if not argv:
		parser.print_help()
		print """
Example: true_model.py -i list.txt

list.txt must contains lines like this for create or associate elements (all elements will be created and/or associated if they don't exist):

My tree > node 1 > element 1
My tree > node 1 > element 2
My tree > node 1 > element 3

If you ONLY want to ASSOCIATE without create elements add | character in front of the element name to garantee that the element exists. Like this:

My tree > node 2 > |element 1
|My tree > |node 2 > element 2
|My tree > |node 2 > |element 3

If you want the search for the element be by similatiry (contains) not iqual (==), use a line like this:

My tree > node 3 > ~element

If the element can't be found a message is will be displayed.

REMEMBER:

 * The element will be created and checked with '--dynamic-identifier' parameter, you may create elements with the same name but different '--dynamic-identifier'.
 * The element association will happen for all the elements found with the search criteria (name: contains or ==)

		"""

		"""
		print "\nExample: %s -i list.txt\n" % ( sys.argv[0] )
		print "list.txt must contains lines like this for create or associate elements (all elements will be created and/or associated if they don't exist):\n"		
		print "My tree > node 1 > element 1"
		print "My tree > node 1 > element 2"
		print "My tree > node 1 > element 3"

		print "\nIf you ONLY want to ASSOCIATE without create elements add | character in front of the element name to garantee that the element exists. Like this:\n"
		print "My tree > node 2 > |element 1"
		print "|My tree > |node 2 > element 2"
		print "|My tree > |node 2 > |element 3"

		print "\nIf you want to search for the element by similatiry (contains not iqual ==), use a line like this:\n"
		print "My tree > node 3 > ~element"

		print "\nIf the element can't be found a message is will be displayed.\nREMEMBER:\n * The element will be created and checked with '--dynamic-identifier' parameter, you may create elements with the same name but different '--dynamic-identifier'.\n * "
		"""

	else:
		args = parser.parse_args()
		DEBUG = args.debug
		LOG.setLevel(DEBUG)

		check_class = args.check_class.split(",")
		dynamic_name = args.dynamic_identifier
		input_file = args.input_file
		cell_name = args.cell_name
		create_class = args.create_class
		ignore_last_id = args.ignore_last_element_identifier
		remove = args.remove
		TRUESIGHT_PATH = args.truesight_path

		env_path = "%s/pw/server/bin/setup_env.sh" % TRUESIGHT_PATH
		if  not os.path.exists( env_path ):
			print "File setup_env.sh (%s) was not found. Please set the correct --truesight-path parameter." % env_path
			exit(1)

		create_elements( input_file, dynamic_name, create_class, check_class, cell_name, ignore_last_id, remove )



if __name__ == '__main__':
  import sys

  main( sys.argv[1:] )



