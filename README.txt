AUTHOR

Frederico Wu - fredericowu@gmail.com


LICENSE

This program is distributed under GPL license, it can be used and modified.


ABOUT

This program was designed to create Truesight model components by reading an input text file which has a structured text.



REQUIREMENTS

This script was tested with Python 2.7 and Truesight/ProactiveNet 9.6.


USAGE

There is a sample input file in the folder 'test'. So you may use like this:

python true_model.py -i test/input.txt -p <Truesight installation path> -cn <cell name>

In my case:
python true_model.py -i test/input.txt -p /opt/ProactiveNet -cn mycell


No lines will be displayed if everything goes ok, only errors are displayed.
After the command be executed, you may check your Truesight Management Administration Console at the Services Editor. Search for the elements and surprise!

After test made, cleanup by adding -rm at the command:

python true_model.py -i test/input.txt -p /opt/ProactiveNet -cn mycell -rm

